/* SPDX-FileCopyrightText: 2023 John Scott <jscott@posteo.net>
 * SPDX-License-Identifier: GPL-2.0-or-later */

/* Try and see if we can fetch a test file from BusyBox httpd */

#define _POSIX_C_SOURCE 200809L
#include <assert.h>
#include <locale.h>
#include <signal.h>
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/socket.h>
#include <sys/un.h>
#include <sys/wait.h>
#include <unistd.h>
#include <curl/curl.h>

int main(void) {
	if(!setlocale(LC_ALL, "")) {
		fputs("Failed to enable default locale\n", stderr);
		exit(EXIT_FAILURE);
	}

	const char *tmpdir = getenv("TMPDIR");
	if(!tmpdir) {
		tmpdir = "/tmp";
	}
	if(setenv("AUTOPKGTEST_TMP", tmpdir, false) == -1) {
		perror("Failed to set environment variable");
		exit(EXIT_FAILURE);
	}
	const char *const autopkgtest_tmp = getenv("AUTOPKGTEST_TMP");
	assert(autopkgtest_tmp);
	if(chdir(autopkgtest_tmp) == -1) {
		perror("Failed to change directory");
		exit(EXIT_FAILURE);
	}

	int sock = socket(AF_UNIX, SOCK_STREAM, 0);
	if(sock	== -1) {
		perror("Failed to open a UNIX domain socket");
		exit(EXIT_FAILURE);
	}
	union {
		struct sockaddr_un sun_addr;
		struct sockaddr sa_addr;
	} addr = { .sun_addr.sun_family = AF_UNIX, .sun_addr.sun_path = "test.S" };
	if(bind(sock, &addr.sa_addr, sizeof(addr)) == -1) {
		perror("Failed to bind UNIX domain socket");
		if(close(sock) == -1) {
			perror("Failed to close socket");
		}
		exit(EXIT_FAILURE);
	}
	if(listen(sock, SOMAXCONN) == -1) {
		perror("Failed to enable listening on socket");
		if(close(sock) == -1) {
			perror("Failed to close socket");
		}
		exit(EXIT_FAILURE);
	}

	const pid_t busybox = fork();
	if(busybox == -1) {
		perror("Failed to fork");
		if(close(sock) == -1) {
			perror("Failed to close socket");
		}
		exit(EXIT_FAILURE);
	}
	if(!busybox) {
		int fd = accept(sock, NULL, NULL);
		if(fd == -1) {
			perror("Failed to accept connection");
			if(close(sock) == -1) {
				perror("Failed to close socket");
			}
			exit(EXIT_FAILURE);
		}
		if(close(sock) == -1) {
			perror("Failed to close socket");
			if(close(fd) == -1) {
				perror("Failed to close socket");
			}
			exit(EXIT_FAILURE);
		}
		if(dup2(fd, STDIN_FILENO) == -1 || dup2(fd, STDOUT_FILENO) == -1) {
			perror("Failed to duplicate file descriptor");
			if(close(sock) == -1) {
				perror("Failed to close socket");
			}
			exit(EXIT_FAILURE);
		}
		if(close(fd) == -1) {
			perror("Failed to close socket");
			exit(EXIT_FAILURE);
		}
		if(execlp("busybox", "busybox", "httpd", "-ifvv", NULL) == -1) {
			perror("Failed to start BusyBox httpd");
			exit(EXIT_FAILURE);
		}
	}
	if(close(sock) == -1) {
		perror("Failed to close socket");
killbusybox:
		kill(busybox, SIGTERM);
		if(waitpid(busybox, NULL, 0) == -1) {
			perror("Failed to wait on BusyBox httpd to terminate");
		}
		exit(EXIT_FAILURE);
	}

	FILE *const testfile = fopen("test.txt", "w");
	if(!testfile) {
		perror("Failed to open index.html");
		goto killbusybox;
	}
	if(fputs("This is a test.\n", testfile) == EOF) {
		perror("Failed to write to test file");
		if(fclose(testfile) == EOF) {
			perror("Failed to close test file");
		}
		goto killbusybox;
	}
	if(fclose(testfile) == EOF) {
		perror("Failed to close test file");
		goto killbusybox;
	}

	/* Now it's time to fetch it with libcurl. */
	CURLcode s = curl_global_init(CURL_GLOBAL_DEFAULT);
	if(s) {
		fprintf(stderr, "Failed to initialize libcurl: %s\n", curl_easy_strerror(s));
		goto killbusybox;
	}
	if(atexit(curl_global_cleanup)) {
		fputs("Failed to register exit handler\n", stderr);
		curl_global_cleanup();
		goto killbusybox;
	}
	CURL *const c = curl_easy_init();
	if(!c) {
		fputs("Failed to get libcurl handle\n", stderr);
		goto killbusybox;
	}
	char *data;
	size_t datalen;
	FILE *const stream = open_memstream(&data, &datalen);
	if(!stream) {
		perror("Failed to open memory stream");
		curl_easy_cleanup(c);
		goto killbusybox;
	}
	char errbuf[CURL_ERROR_SIZE];
	if((s = curl_easy_setopt(c, CURLOPT_VERBOSE, 1L))
	|| (s = curl_easy_setopt(c, CURLOPT_WRITEDATA, (void *)stream))
	|| (s = curl_easy_setopt(c, CURLOPT_ERRORBUFFER, errbuf))
	|| (s = curl_easy_setopt(c, CURLOPT_FAILONERROR, 1L))
	|| (s = curl_easy_setopt(c, CURLOPT_URL, u8"http://test/test.txt"))
	|| (s = curl_easy_setopt(c, CURLOPT_UNIX_SOCKET_PATH, "./test.S"))) {
		fprintf(stderr, "Failed to set libcurl option: %s\n", curl_easy_strerror(s));
closestream:
		if(fclose(stream) == EOF) {
			perror("Failed to close memory stream");
		} else {
			free(data);
		}
		curl_easy_cleanup(c);
		goto killbusybox;
	}

	s = curl_easy_perform(c);
	if(s) {
		fprintf(stderr, "Failed to fetch content with libcurl: %s: %s\n", curl_easy_strerror(s), errbuf);
		goto closestream;
	}
	curl_easy_cleanup(c);
	if(fclose(stream) == EOF) {
		perror("Failed to close memory stream");
		goto killbusybox;
	}
	if(strcmp(data, "This is a test.\n")) {
		fprintf(stderr, "Unexpectedly received message '%s'\n", data);
		free(data);
		goto killbusybox;
	}
	free(data);

	siginfo_t info = {0};
	if(waitid(P_PID, busybox, &info, WEXITED) == -1) {
		perror("Failed to wait for BusyBox to terminate");
		exit(EXIT_FAILURE);
	}
	if(info.si_code != CLD_EXITED || info.si_status != EXIT_SUCCESS) {
		psiginfo(&info, "BusyBox httpd terminated abnormally");
		exit(EXIT_FAILURE);
	}
	/* success */
}
